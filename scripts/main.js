window.onload = function() {
    var color_plateR = document.getElementById('color_plateR');
    //color_plateRc=color_plateR.getContext('2d');
    var color_plateG = document.getElementById('color_plateG');
    //color_plateGc=color_plateG.getContext('2d');
    var color_plateB = document.getElementById('color_plateB');
    //color_plateBc=color_plateB.getContext('2d');
    var color_plate = document.getElementById('color_plate');
    color_platec=color_plate.getContext('2d');
    var current_color = document.getElementById('current_color');
    var reset = document.getElementById('reset');
    var LineWidth = document.getElementById('line_width');
    var lineWidthValue= document.getElementById('lineWidthValue');
    var frames= document.getElementById('frame');
    var update_time= document.getElementById('update_times');
    var save= document.getElementById('download');
    var input_text= document.getElementById('input_text');    
    var font_size= document.getElementById('font_size');
    var fontsize= document.getElementById('fontsize');
    var upload_file=document.getElementById("file");
    

    var color_style;
    var frame = new Array();
    var frame_index=-1;
    var update_times=0;
    var tempImage = new Image();



    var sr=0,sg=0,sb=0;
    color_platec.fillStyle=color_style;
    current_color.textContent=color_style;
    color_platec.fillRect(0,0,40,40);    
    color_plateR.oninput = function() {
        sr=this.value;
        color_style='rgb('+sr+','+sg+','+sb+')';
        color_platec.fillStyle=color_style;
        current_color.textContent=color_style;
        color_platec.fillRect(0,0,40,40);        
    }
    color_plateG.oninput = function() {
        sg=this.value;
        color_style='rgb('+sr+','+sg+','+sb+')';
        color_platec.fillStyle=color_style;
        current_color.textContent=color_style;
        color_platec.fillRect(0,0,40,40);        
    }
    color_plateB.oninput = function() {
        sb=this.value;
        color_style='rgb('+sr+','+sg+','+sb+')';
        color_platec.fillStyle=color_style;
        current_color.textContent=color_style;
        color_platec.fillRect(0,0,40,40);        
    }
    lineWidthValue.innerHTML=LineWidth.value;
    LineWidth.oninput = function() {
        lineWidthValue.innerHTML=this.value;
      }
      fontsize.innerHTML=font_size.value;
      font_size.oninput = function() {
        fontsize.innerHTML=this.value;
      }      

    var canvas,ctx,tempCanvas,tempCtx,canvasContainer;
    var toolButtons = document.querySelectorAll(".tool");


    canvas = document.getElementById('myCanvas');
    ctx = canvas.getContext('2d');
    canvasContainer=canvas.parentNode;
    tempCanvas = document.createElement('canvas');
    tempCanvas.id='tempCanvas';
    canvasContainer.appendChild(tempCanvas);
    tempCtx = tempCanvas.getContext('2d');
    tempCanvas.width=canvas.width;
    tempCanvas.height=canvas.height;

    chosetool();
    ctx.fillStyle='white';
    ctx.fillRect(0,0, tempCanvas.width, tempCanvas.height);
    
    reset.onclick=function(){
        ctx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
        frame_index=-1;
        frame.length=0;
        for(let kj=0; j<toolButtons.length; k++){
            avi[k]=false;
            toolButtons[k].classList.remove('selected');
        }     
        avi[0]=true;
        toolButtons[0].classList.add('selected');
    };
    save.onclick=function(){
        var saveImage=document.createElement('a');
        saveImage.href=canvas.toDataURL();
        saveImage.download='pic.jpeg';
        saveImage.click();  
     };
    var cursors=['Pencil','Eraser','Line','Rectangle','Circle','Traingle','Text'];
    var current_tool={'pencil':pencil,'eraser':eraser,'line':line,'rect':rect,'circle':circle,'triangle':triangle,'undo':undo,'redo':redo};
    var name;
    var avi=[true,false,false,false,false,false,false,false,false,false];

    function chosetool(){
        for(let i=0; i<toolButtons.length; i++){
            toolButtons[i].addEventListener('click', function(){
            for(let j=0; j<toolButtons.length; j++){
                avi[j]=false;
                toolButtons[j].classList.remove('selected');
            }                
            avi[i]=true;            
            toolButtons[i].classList.add('selected');    
            if(i<7){
                tempCanvas.style.cursor='url(images/cursors/'+cursors[i]+'.cur),auto';       
            }
            });
        }
    }
    

    toolButtons[7].onclick= function undo(){
        if (frame_index >= 0 && avi[7])   
        {              
            frame_index=frame_index-1;                                                              
            tempImage.src = frame[frame_index]; 
            if(frame_index==-1)
                ctx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);        
            else {
                tempImage.onload=function(){               
                    ctx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
                    ctx.drawImage(tempImage, 0, 0,tempCanvas.width, tempCanvas.height);
                }       
            }     
            frames.textContent=frame_index;
        }  
    }

    toolButtons[8].onclick= function redo(){
        if (frame_index <frame.length-1  &&avi[8])     
        {               
            frame_index=frame_index+1;                               
            tempImage.src = frame[frame_index];
            tempImage.onload=function(){
                ctx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);                
                ctx.drawImage(tempImage, 0, 0,tempCanvas.width, tempCanvas.height); 
            }
            frames.textContent=frame_index;
        }  
    }



    function temp_update () {
        ctx.drawImage(tempCanvas, 0, 0);
        tempCtx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);              
        frame_index=frame_index+1;
        if(frame.length>frame_index)
            frame.length=frame_index;              
        frame[frame_index]=canvas.toDataURL();  
        
    }

    function getMousePos(tempCanvas, event) {
        var rect = tempCanvas.getBoundingClientRect();
        return {
          x: event.clientX - rect.left,
          y: event.clientY - rect.top
        };
      }

    
        
    var pencilStart=false;
    var eraserStart=false;
    var lineStart=false;
    var circleStart=false;
    var rectStart=false;
    var triangleStart=false;
    var TextStart=false;

    $(tempCanvas).mousedown(function (event){ 
        if(avi[0]){
            
            var mousePos = getMousePos(tempCanvas, event);
            pencilStart=true;
            tempCtx.beginPath();
            tempCtx.moveTo(mousePos.x,mousePos.y);            
        }
        if(avi[1]){
            var mousePos = getMousePos(tempCanvas, event);
            eraserStart=true;
            tempCtx.beginPath();
            tempCtx.moveTo(mousePos.x,mousePos.y);
        }
        if(avi[2]){
            var mousePos = getMousePos(tempCanvas, event);
            lineStart=true;
            begin_x=mousePos.x;
            begin_y=mousePos.y;
        } 
        if(avi[4]){
             var mousePos = getMousePos(tempCanvas, event);
            circleStart=true;
            begin_x=mousePos.x;
            begin_y=mousePos.y;
        }
        if(avi[3]){
            var mousePos = getMousePos(tempCanvas, event);
            rectStart=true;
            begin_x=mousePos.x;
            begin_y=mousePos.y;
        }
        if(avi[5]){
            var mousePos = getMousePos(tempCanvas, event);
            triangleStart=true;
            begin_x=mousePos.x;
            begin_y=mousePos.y;
        }
        if(avi[6]){
            TextStart=true;
        }        
    });
    $(tempCanvas).mousemove(pencilmove=function(event){
        if(pencilStart &&avi[0]){
            var mousePos = getMousePos(tempCanvas, event);
            tempCtx.strokeStyle =color_style;
            tempCtx.lineTo(mousePos.x,mousePos.y);
            tempCtx.lineWidth = LineWidth.value;
            tempCtx.stroke();
        }
        if(eraserStart　&&avi[1]){
            var mousePos = getMousePos(tempCanvas, event);
            tempCtx.strokeStyle = 'white';
            tempCtx.lineWidth = LineWidth.value;
            tempCtx.lineTo(mousePos.x,mousePos.y);                
            tempCtx.stroke();
        }
        if(lineStart &&avi[2]){
            tempCtx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
            tempCtx.beginPath();
            var mousePos = getMousePos(tempCanvas, event);
            tempCtx.moveTo(begin_x,begin_y);
            tempCtx.lineTo(mousePos.x,mousePos.y);
            tempCtx.strokeStyle =color_style;
            tempCtx.stroke();
            tempCtx.closePath();                
        }
        if(circleStart&&avi[4]){
            tempCtx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
            tempCtx.beginPath();
            var mousePos = getMousePos(tempCanvas, event);
            tempCtx.moveTo(begin_x,begin_y);
            w = Math.abs(begin_x - mousePos.x);
            h = Math.abs(begin_y - mousePos.y);
            r = Math.max(w,  h);
            tempCtx.arc(begin_x,begin_y,r,0,2*Math.PI);
            tempCtx.fillStyle =color_style;
            tempCtx.fill();
            tempCtx.closePath();
        }
        if(rectStart&&avi[3]){
            tempCtx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
            var mousePos = getMousePos(tempCanvas, event);
            tempCtx.beginPath();
            tempCtx.moveTo(begin_x,begin_y);
            w = Math.abs(begin_x - mousePos.x);
            h = Math.abs(begin_y - mousePos.y);
            x = Math.min(begin_x,mousePos.x);
            y = Math.min(begin_y,mousePos.y);               
            tempCtx.rect(x,y,w,h);
            tempCtx.fillStyle =color_style;
            tempCtx.fill();
            tempCtx.closePath();
        }
        if(triangleStart&&avi[5]){
            tempCtx.clearRect(0, 0, tempCanvas.width, tempCanvas.height);
            var mousePos = getMousePos(tempCanvas, event);
            tempCtx.beginPath();
            tempCtx.moveTo(begin_x,begin_y);
            w = Math.abs(begin_x - mousePos.x);
            h = Math.abs(begin_y - mousePos.y);
            r = Math.max(w,  h);
            if(begin_y - mousePos.y <0){
                
                tempCtx.moveTo(begin_x,begin_y-r);
                tempCtx.lineTo(begin_x+r/2*3/1.732,begin_y+r);
                tempCtx.lineTo(begin_x-r/2*3/1.732,begin_y+r);
                tempCtx.lineTo(begin_x,begin_y-r);
            }
            else{
                tempCtx.moveTo(begin_x,begin_y+r);
                tempCtx.lineTo(begin_x+r/2*3/1.732,begin_y-r);
                tempCtx.lineTo(begin_x-r/2*3/1.732,begin_y-r);    
                tempCtx.lineTo(begin_x,begin_y+r);            
            }
            tempCtx.fillStyle =color_style;
            tempCtx.strokeStyle =color_style;
            tempCtx.lineWidth='1';
            tempCtx.stroke();
            tempCtx.fill();
            tempCtx.closePath();
        }        
    });
    $(tempCanvas).mouseup(function(event){
        if(pencilStart &&　avi[0]){
        tempCtx.closePath();
        pencilStart=false;
        temp_update();                                
        }
        if(eraserStart&&avi[1]){
        tempCtx.closePath();
        eraserStart=false;
        temp_update();                   
        }
        if(lineStart&&avi[2]){
            temp_update();
            lineStart=false;                
        }
        if(circleStart&&avi[4]){
            temp_update();
            circleStart=false;                 
        }
        if(rectStart&&avi[3]){
            temp_update();
            rectStart=false;             
        }
        if(triangleStart&&avi[5]){
            temp_update();
            triangleStart=false;             
        }
        if(TextStart&&avi[6]){
            var mousePos = getMousePos(tempCanvas, event);
            tempCtx.font=font_size.value+'px Arial';  
            tempCtx.fillStyle =color_style; 
            tempCtx.fillText(input_text.value,mousePos.x,mousePos.y);
            temp_update();
            TextStart=false;             
        }
    });
        

    upload_file.addEventListener('change',function(){
        var reader = new FileReader();
        var img_upload = new Image();
        if(this.files && this.files[0]){
          reader.addEventListener('load',function(event){
            img_upload.addEventListener('load',function(){
              tempCtx.drawImage(img_upload,0,0);
              url = img_upload.src;
              push_url();
              console.log(url_array[0]);
              console.log(url_array[1]);
              console.log(url_array.length);
              console.log(url_n);
            });
            img_upload.src = event.target.result;
          });
          reader.readAsDataURL(this.files[0]);
        }
        console.log(url_array[0]);
        console.log(url_array[1]);
        console.log(url_array.length);
        console.log(url_n);
      });    
      temp_update();
}
